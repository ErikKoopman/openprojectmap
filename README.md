# Open Project Map

The open project map is a compact template and roadmap to visualize and communicate projects in all areas of expertise in a same way. 

Goal of this Gitlab project is to develop the open project map into:
1. an open standard for project communication
2. widgets for direct use in (intranet) websites
3. an online platform that supports companies, governments, schools etc. to do open projects

## Open standard for project communication

Organizations, as companies and governments, do a wide variety of projects. The problem is: every area of expertise and every project management method uses various terminology for the same things. There is no open standard for project communication.

The open project map is a universal road map and template for standardizing project communication to:
- connect various project terminology with the same purpose to clear descriptions in plain English.
- give a compact overview of all project steps from idea to end of project, with all steps ranked in order of constructive decision making
- offer direct links to the main activities and project documentation
- show the status of activities and project documentation
- visualize maps for both conventional (waterfall) and iterative (agile) project development

[Example: Open project map for construction project](improve_basic_templates/examples/Project-template-Construction.pdf)  
[Example: Open project map for IT-project (agile)](improve_basic_templates/examples/Project-template-IT-Agile.pdf)

### Better participation

Aim of the open project map is to give direct project overview and fast access to project documentation to:
- end users as clients, employees, citizens
- knowledge workers as project members, developers, advisers, experts 
- decision makers as executives, councillors, representatives
- other stakeholders, who have no access to project files

Areas of focus are:
- multi stakeholder participation, open innovation and open organization
- citizen participation and (starting with) open government

### How it works

The open project map links terminology for project documents and activities to often used projects steps with the same aim, purpose and focus.

Example:

Example 1:  
![Explanation of the open project map step 1](improve_basic_templates/examples/images/Explanation_project_step_1.png)

Example 2:  
![Explanation of the open project map step 1](improve_basic_templates/examples/images/Explanation_project_step_6.png)

All 24 visualized boxes in the open project map work this way. Everyone is free to use whatever terminology they want, if its relevant for constructive project development its connected to a purpose and focus, and mapped in plain English in the open project map. 

The open project map offers routes for both waterfall and agile project management

For more information: see [detailed explanation](improve_basic_templates/detailed_explanation_of_the_open_project_map.md) and [overview of terminology for project documents](improve_basic_templates/examples/overview-terminoly-to-purpose-and-project-steps.pdf).

### Improve, promote, translate

The first goal of this Gitlab project is to improve, promote and translate the open project map, and make it internationally known.

Be welcome to join these (non-code) activities.

---

## Widgets for direct use in (intranet) websites

![Widgets for the open project map](build-online-platform/mock-ups-and-prototypes/Widgets.png)

Integration in (project) websites is an importent step to make the open project map known and operatable. Widgets are a good way. 

So please develop a widget.


---

## Online platform for open projects

Have a whole project at your fingertips. That’s what the online platform is meant to do for developers, end users, decision makers and other stakeholders.

Adding the open project map to a menu bar makes it possible to oversee the status of a project directly, and have direct access to the main project documents and activities.

![Example of the micro road map of the open project map in the menu bar](build-online-platform/mock-ups-and-prototypes/Introduction_menu_bar.png)

### Test the idea

Click the boxes in the below ‘micro road map’ to test the idea behind the open project map yourself. The boxes show the main documents and activities belonging to this project.

[Click here for the full map](/build-online-platform/open-project-map-for-this-project/Project-template-OPM.png)

![Open project map Initiation](/build-online-platform/open-project-map-for-this-project/Icons/I.png "Initiation")
[![Open project map Initiation Put on agenda - basis](/build-online-platform/open-project-map-for-this-project/Icons/0-1.png "Initiation: Put on agenda - basis")](/build-online-platform/open-project-map-for-this-project/0-1_Initiation-Put-on-agenda-basis_Idea.md)
[![Open project map Initiation Put on agenda - detail](/build-online-platform/open-project-map-for-this-project/Icons/0-2.png "Initiation: Put on agenda - detail")](/build-online-platform/open-project-map-for-this-project/0-2_Initiation-Put-on-agenda-detail_Arguments.md)
[![Open project map Initiation Project goals - basis](/build-online-platform/open-project-map-for-this-project/Icons/0-3.png "Initiation: Project goals - basis")](/build-online-platform/open-project-map-for-this-project/0-3_Initiation-Project-goals-basics_Intention.md)
[![Open project map Initiation Project management - preparation](/build-online-platform/open-project-map-for-this-project/Icons/0-4.png "Initiation: Project management - preparation")](/build-online-platform/open-project-map-for-this-project/0-4_Initiation-Project-management-preparation_Project-startup.md)

![Open project map Definition](/build-online-platform/open-project-map-for-this-project/Icons/D.png "Definition")
[![Open project map Definition Analysis - basis](/build-online-platform/open-project-map-for-this-project/Icons/1-1.png "Definition: Analysis - basis")](/build-online-platform/open-project-map-for-this-project/1-1_Definition-Analysis-basis_Stakeholder-analysis.md)
[![Open project map Definition Analysis - detail](/build-online-platform/open-project-map-for-this-project/Icons/1-2.png "Definition: Analysis - detail")](/build-online-platform/open-project-map-for-this-project/1-2_Definition-Analysis-detail_Situation-analysis.md)
[![Open project map Definition Project goals - detail](/build-online-platform/open-project-map-for-this-project/Icons/1-3.png "Definition: Project goals - detail")](/build-online-platform/open-project-map-for-this-project/1-3_Definition-Project-goals-detail_Deliverables.md)
[![Open project map Definition Project management - basis](/build-online-platform/open-project-map-for-this-project/Icons/1-4.png "Definition: Project management - basis")](/build-online-platform/open-project-map-for-this-project/1-4_Definition-Project-management-basis_Product-vision.md)

![Open project map Design](/build-online-platform/open-project-map-for-this-project/Icons/D.png "Design")
[![Open project map Design Project management - detail](/build-online-platform/open-project-map-for-this-project/Icons/2-1.png "Design: Project management - detail")](/build-online-platform/open-project-map-for-this-project/2-1_Design-Project-management-detail_Product-backlog.md)
[![Open project map Design Requirements - basis](/build-online-platform/open-project-map-for-this-project/Icons/2-2.png "Design: Requirements - basis")](/build-online-platform/open-project-map-for-this-project/2-1_Design-Project-management-detail_Product-backlog.md)
[![Open project map Design Requirements - detail](/build-online-platform/open-project-map-for-this-project/Icons/2-3.png "Design: Requirements - detail")](/build-online-platform/open-project-map-for-this-project/2-3_Design-Requirements-detail_Acceptation-criteria.md)
[![Open project map Design Project management - basis](/build-online-platform/open-project-map-for-this-project/Icons/2-4.png "Design: Design - basis")](/build-online-platform/open-project-map-for-this-project/2-4_Design-Development-basis_Prototypes-and-Functional-design.md)

![Open project map Development](/build-online-platform/open-project-map-for-this-project/Icons/D.png "Development")
[![Open project map Development Development - detail 30%](/build-online-platform/open-project-map-for-this-project/Icons/3-1.png "Development: Development - detail 30%")](/build-online-platform/open-project-map-for-this-project/3-1_Development-Development-detail-30%25_Minimum-viable-product.md)
[![Open project map Development Development - detail 70%](/build-online-platform/open-project-map-for-this-project/Icons/4-1.png "Development: Development - detail 70%")](/build-online-platform/open-project-map-for-this-project/4-1_Development-Development-detail-70%25_20-80-version.md)
[![Open project map Development Development - detail >85%](/build-online-platform/open-project-map-for-this-project/Icons/4-1.png "Development: Development - detail >85%")](/build-online-platform/open-project-map-for-this-project/5-1_Development-Development-detail-85%25_Rolling-updates.md)
[![Open project map Development Development - transfer](/build-online-platform/open-project-map-for-this-project/Icons/6.png "Development: Development - transfer")](/build-online-platform/open-project-map-for-this-project/6-1_Development-Development-transfer_End-of-development.md)

![Open project map Realization](/build-online-platform/open-project-map-for-this-project/Icons/R.png "Realization")
[![Open project map Realization Implementation - preparation](/build-online-platform/open-project-map-for-this-project/Icons/3-2.png "Realization: Implementation - preparation")](/build-online-platform/open-project-map-for-this-project/3-2_Realization-Implementation-preparation_Setup-serverside.md)
[![Open project map Realization Implementation - basis](/build-online-platform/open-project-map-for-this-project/Icons/4-2.png "Realization: Implementation - basis")](/build-online-platform/open-project-map-for-this-project/4-2_Realization-Implementation-basis_Steady-server.md)
[![Open project map Realization Implementation - detail](/build-online-platform/open-project-map-for-this-project/Icons/4-2.png "Realization: Implementation - detail")](/build-online-platform/open-project-map-for-this-project/5-2_Realization-Implematation-detail_Optimalization.md)
[![Open project map Realization Implementation - transfer](/build-online-platform/open-project-map-for-this-project/Icons/6.png "Realization: Implementation - transfer")](/build-online-platform/open-project-map-for-this-project/6-2_Realization-Implematation-transfer_Hand-over-keys.md)

![Open project map Use & Maintain](/build-online-platform/open-project-map-for-this-project/Icons/U.png "Use & Maintain")
[![Open project map Use & Maintain Use - basis](/build-online-platform/open-project-map-for-this-project/Icons/3-3.png "Use & Maintain: Use - basis")](/build-online-platform/open-project-map-for-this-project/3-3_Use-and-Maintain-Use-basis_Testing%20the%20product.md)
[![Open project map Use & Maintain Use - detail](/build-online-platform/open-project-map-for-this-project/Icons/4-3.png "Use & Maintain: Use - detail")](/build-online-platform/open-project-map-for-this-project/4-3_Use-and-Maintain-use-detail_Serious-product.md)
[![Open project map Use & Maintain Aftercare](/build-online-platform/open-project-map-for-this-project/Icons/4-3.png "Use & Maintain: Aftercare")](/build-online-platform/open-project-map-for-this-project/5-3_Use-and-Maintain-Aftercare_Updates.md)
[![Open project map Use & Maintain End of project](/build-online-platform/open-project-map-for-this-project/Icons/6.png "Use & Maintain: End of project")](/build-online-platform/open-project-map-for-this-project/6-3_Use-and-Maintain-End-of-project_Project-closure.md)


### Walk the talk

The online platform for open projects is build from the bottom up using the open project map (agile route) as basis for communication, development and decision making.

In the open project map for this open project you see that many steps are “open for input”. These steps have to be developed together. So please bring in your ideas and knowledge. 

At the same time you can start to code. These items are in the product backlog:
    1. develop a menu bar with an integrated open project map to use for this project
    2. connect tools as Etherpad, wiki, MS Word, Google docs or just pdf to the open project map in the menu bar (see mock ups)

### Building a “Wordpress” for open projects

The long term vision is to develop a “Wordpress” for open projects, an online platform that is available for everyone. An open platform that offers all the tools, templates and knowledge to effectively develop open projects and democratize project development.

## About the initiator

The open project map is developed by Erik Koopman, an electrical engineer and social innovation designer from The Netherlands (with no coding experience). Erik is a huge believer of openness as basis for a free, fair and peaceful world – and a well functioning democracy and government.

The first design of the open project was made because a citizen participation project by the local government was unclear and full of terminology, which was a nice challenge for a social innovation designer to improve that situation. 

I like to read your comments and hope we can build together a beautiful and effective tool for open projects. Have fun.

## LICENSE
The basic templates of the Open project map are licensed as [Creative Commons BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/).
The online platform is licenced as "what will be decided in “project goals - detail”





