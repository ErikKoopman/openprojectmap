# Detailed explanation of the open project map

## Introduction

The open project map is 'in alpha'. Please help to improve the templates

### Why the open project map was developed 

The open project map is developed by [Erik Koopman](https://www.linkedin.com/in/erikkoopman/). I am an electrical engineer (work) and [social innovation designer](http://erikkoopman.nl/) (passion). I love to develop tools that focus on openness, because I deeply belief that more openness of systems and processes is the fastest and most constructive way to a better world.

The open project map is a result of:
1. my interest in a citizen participation project for the redevelopment of a city area. There was all sorts of governmental terminology. The process was not transparent. And I didn’t understand how the project would proceed.
2. my doodling with terminology from various areas of expertise and making the connection to stages and purpose
3. my belief that open projects are a solid basis for the future of democracy.

### Today’s participation processes 

Co-creation, open innovation, citizen participation, open source development and other forms of multi-stakeholder participation are a growing practice in business and government. It works!

Participation projects however are for many stakeholders not easy because:
- every area of expertise, organization and project is different
- use of different methods for project management
- terminology
- long throughput time of projects
- most processes are not transparent
- most documentation is not accessible or difficult to find for non-insiders
- there is no open standard for project communication

In sum: participation projects often face lack of overview, lack of clear communication and lack of transparency.

### Open project map, an open standard for project communication

Open project map stands for:  
 **_Open_** *Accessible and transparent*  
 **_Project_**	*All project steps from idea to use of a product, tool or service*  
 **_Map_** *Overview, road map, folder (translation from dutch ‘map’)*

The open project map is a model that connects terminology from all areas of expertise and project management to project steps with a same purpose in plain English. 

Example 1:  
![Explanation of the open project map step 1](examples/images/Explanation_project_step_1.png)

Example 2:  
![Explanation of the open project map step 1](examples/images/Explanation_project_step_6.png)

Example 3:  
![Explanation of the open project map step 1](examples/images/Explanation_project_step_13.png)

Connecting terminology to project steps with a same purpose in plain English clears up project management and project communication for many people.

Because the open project map can be used for a wide variety of projects, it has a decent basis to become an open standard.

## Basics of the open project map

### The six stages of a project

The open project map visualizes six stages of project development:  
**_I_**  *Initiate*  
**_D_**  *Define*  
**_D_**  *Design*  
**_D_**  *Develop (or purchase)*  
**_R_**  *Realize / Roll-out*  
**_U_**  *Use & Maintain*

The boxes of ‘Use & Maintain’ are bold. As reminder that its the objective of the project.

### The four project steps of every stage

Every stage in the open project map follows the wellknown PDCA-cycle:

**_Plan_** / *Coordinate*  
**_Do_** / *Build*  
**_Check_** / *Anchor (results)*  
**_Act/Adjust_** / *Convert/Transfer to next stage*

<sub>The PDCA-steps where not part of the design process of the open project map, afterwards this order turned out to be part of all the stages.</sub>

> #### Together there are 6 x 4 = 24 purposes / project steps, which are wise to look at for effective project development and constructive decision making.

### Purpose : the main activities in projects

Terminology in the open project map is connected to purpose. 

The main groups of purpose are:
- Put on agenda
- Project goals
- Project management
- Analysis
- Requirements
- Design
- Development
- Realization
- Use
- Aftercare
- End of project

### Focus: Basis and Detail

In the open project map first the basis is decided, then the details are worked out. In constructive project development this practice is common.

The connection between basis and detail is visualized by a line between two boxes.

When three boxes are connected by a line, it shows the workflow of conventional/waterfall (horizontal lines) or iterative/agile (vertical lines) project development, see below.

<sub>In two cases is ‘preparation’ part of the workflow. In most cases however is the ‘detail’ of a purpose the preparation for the ‘basis’ of the next purpose.</sub>

### Direct links to main activities and documentation

The open project map is a universal template. Its the terminology that makes the connection to an area of expertise or objective of an activity or document. Therefor terminology is used for the link to activities and documentation.

### Status of activities and documentation

Asking for input, feedback and consent of stakeholders is a main part of participation projects and the open project map.

Advised is to make all project steps in the stages “Define’ and ‘Design’ from the beginning ‘open for input’. It supports the development of the project. 

For example: ‘open for input’ makes it possible to copy important lessons from an analysis directly to the requirements (so they won’t be forgotten), as well, an early mentioned requirement can help to deepen and improve an analysis.

The status of project steps in participation projects can be:
    • open for input
    • concept 
    • feedback
    • final 
    • public evaluation
    • decision process
    • decided
    • on hold
    • revision A etc.

Version numbers and date should always be added.

### Route for conventional (waterfall) project management

Conventional project management is a relatively linear sequential approach for project development ("downwards" like a waterfall). It originated in the manufacturing and construction industries but is used in many areas of expertise and (hierarcical) organizations.

The open project map visualizes the conventional approach using horizontal lines in the stages development, realization and use & maintain.

![Explanation planning open project map for conventional project development](examples/images/planning-open-waterfall-project-map.png)

### Route for iterative and incremental (agile) project management

Iterative and incremental (agile) project management is method is to develop a system, product, tool or service through repeated cycles (iterative) and in smaller portions at a time (incremental). It originated in software development but is used in many areas of expertise, mostly by teams.

The open project map visualizes the iterative and incremental approach using vertical lines in the stages development, realization and use & maintain.

![Explanation planning open project map for iterative and incremental project development](examples/images/planning-open-agile-project-map.png)

The first vertical row includes the sprints needed for making a minimum viable product or equal.  
The second vertical row includes the sprints needed for making a product that is 70% ready.  
The third vertical row includes the sprints needed for optimizing a product or equal.


## Formats of the open project map

The open project map can be used and viewed in five ways:
    1. List, a simple overview of main project documentation
    2. Compact road map, fits on one screen or A4 / letter
    3. Micro road map, fits on a thumbnail / widget / menu bar / corner of a document cover page
    4. Canvas, fits on one screen and 2x A4 / letter
    5. Public folder / Repository (as Gitlab), not explained in this document

The list and road map offer project overview in a blink with direct access to main activities and documentation

### List

A compact road map can be difficult at first for many people. For this reason a simple overview of purpose with connected activities and project documentation can be made, for example:

| Purpose                | Documentation          | Status                 |
| :--------------------- | :--------------------- | :--------------------- |
| ▼  Project goals       |                        |                        |
| .  *basis*             | Project mandate        | Decided: yyyy, mmm dd  |
| .  *detail*            | Deliverables           | Decided: yyyy, mmm dd  |
| ▼  Analysis            |                        |                        |
| .  *basis*             | User analysis          | Decided: yyyy, mmm dd  |
| .  *detail*            | Feasibility study      | Decided: yyyy, mmm dd  |
| ▼  Project management  |                        |                        |
| .  *preparation*       | Project start up       | Decided: yyyy, mmm dd  |
| .  *basis*             | Project plan           | Concept                |
| .  *detail*            | Planning               | Concept                |
| ►  Requirements        |                        | Open for input         |
| ►  Development         | On hold                | planned yyyy, mmm      |
| ►  Roll out            | On hold                | not planned yet        |
| ►  Use & Maintain      | On hold                | goal yyyy, mmm         |  

The activities and project documentation in the above list can be linked to the same project steps as described in the road map below.

### Compact road map

The compact road map fits on one screen or A4/letter

![Example compact template of the open project map](examples/images/Project-template-IT-Agile.png)

### Micro road map

The micro road map fits on a thumbnail / widget / menu bar / corner of a document cover page

![Example micro template of the open project map](examples/images/Micro_road_map.png)

### Canvas

![Example canvas of the open project map](examples/images/Canvas.png)