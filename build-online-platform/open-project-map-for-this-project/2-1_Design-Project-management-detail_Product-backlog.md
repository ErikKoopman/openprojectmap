2.1 Design - Project management detail

Open for input

# Product backlog

## Improve open project map

1. Discuss the quality of the open project map
2. Add new terminology to the open project map


## Define

1. Do stakeholder analysis
2. Do situation analysis


## Design 

1. Show your ideas in a mock up
2. Discuss what is the best tool for a public product backlog that both developers and non-developers can effectively work with


## Develop a prototype

1. Develop menu bar with integrated micro open project map
2. Add viewport to menu bar
3. Add top bar