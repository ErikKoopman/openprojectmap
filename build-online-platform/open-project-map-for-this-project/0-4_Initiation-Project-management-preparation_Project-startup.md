0.4 Initiation - Project management preparation

# Project start up

As developer of the open project map I have no experience with coding and Gitlab or equal platforms. So, for me, its jump in the river and learn to swim. I hope you gonna forgive all my sin.

I want to use the open project map as basis for project management and project development.

So the next steps are:
- Analsis basis - > Stake holder analsis, see questions in the Issues, to test the support for this project
- Analysis detail -> Situation analysis, what tools are around, where are chances?
- Project goals detail -> Deliverables, which will be decided collectively
- Requirements basis -> Defintion of Done
- Requirements detail -> Acceptation criteria
These steps are open for input.

At the same time open for input is:
- Design basis -> Prototyping, sent in your code or mock ups

Erik Koopman
January 2019