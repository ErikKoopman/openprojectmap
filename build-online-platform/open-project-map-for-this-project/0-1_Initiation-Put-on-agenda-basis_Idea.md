0.1 Initiation - Put on agenda main

# Idea

The [open project map](https://gitlab.com/ErikKoopman/openprojectmap/blob/master/README.md) is a template to communicate projects in all area of expertise in a same way. What it needs to become a success is online tools to make implementation easy and effective.

Next to online templates offers a micro version of the open project map in a menu bar the possibility to have direct and full oversight of a project, by linking directly to the main project steps and documentation.

Both, the online templates and an online platform is what I would like to be developed.

Erik Koopman
December 2018
