2.4 Design - Development basis

Open for input

# Prototypes & Functional design

Show your code for prototypes, mock ups and ideas for functional design here.

Please add a screenshot with a link to your work. Your work can be on your server or put in the folder [mock-ups-and-prototypes](https://gitlab.com/ErikKoopman/openprojectmap/tree/master/build-online-platform/mock-ups-and-prototypes).

The functional design has to focus on future use of the online open project map in multiple projects in organizations and a public collaboration platform.

The development process is:
1. Open for input: collecting (code for) prototypes, mock ups and ideas for functional design
1. Concept: testing protypes and making a concept functional design
1. Decide: set the basis for the minimum viable product