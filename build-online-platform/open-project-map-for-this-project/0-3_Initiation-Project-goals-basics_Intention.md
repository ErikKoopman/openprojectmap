0.3 Initiation - Project goals basics

# Intention

Development of the open project map is focusing on: 
- Improve the OPM-model (review, correct, extent, live tests)
- Build online templates
- Build an open online platform that facilitates open projects, improves democracy and leads to better projects, products and services for end users
- Become a standard tool in every area of expertise. Pass Gitlab and Github as platform for open projects

These goals are my intention for the project. In 'Project goals - detail' the project goals will be decided collectively.

Erik Koopman
December 2018
