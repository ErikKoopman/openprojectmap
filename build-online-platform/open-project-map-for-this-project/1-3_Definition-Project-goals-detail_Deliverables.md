1.3 Definition - Project goals detail

Open for input

# Deliverables

Deliverables will be collectively set after the stakeholder analysis and situation analysis. Till then see [Project goals basis -> Intention](https://gitlab.com/ErikKoopman/openprojectmap/blob/master/build-online-platform/open-project-map-for-this-project/0-3_Initiation-Project-goals-basics_Intention).

Be free to mention the deliverables you want to see.