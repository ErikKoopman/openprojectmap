1.2 Definition - Analysis detail

Open for input

# Situation analysis

## Main questions

Reinventing the wheel is not the goal of this project. So 2 questions have to be answered first:

1. What are equal templates to the open project map?
2. What are equal tools to the micro open project map in the menu bar?

## Questions about tools for online participation

Open projects have 4 basic parts

- Asking questions to all stakeholders, and collecting and reviewing answers
- Collaboration, publicly work on the project
- Collective decision making
- Access to documentation and work

The questions below focus on multi-stakeholder participation and citizen participation, not on collaboration in teams.

### Tools for asking questions

- What are good examples of asking questions online?
- What are good tools for asking questions online?
- What are good tools to order answers?
- What are good tools to review answers?

### Tools for online collaboration

What are good tools to collectively and publicly make a decent situation analysis?
What are good tools for public online collaboration for non-developers?
What are great open source tools that can be used/integrated in this project
What tools are used in multi-stakeholder participation and citizen participation?

### Tools for collective decision making

What are good tools to collectively and publicly make decisions?

### Tools for open access to documentation and work

What tools are there for public access to documentation and work?

### Extra

What is a good template to write the concept for this situation analysis?