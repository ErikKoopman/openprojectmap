0.2 Initiation - Put on agenda detail

# Arguments

Development of the open project map is promising because: 
- No equal models or standards are found on the internet
- Project management is shattered with terminology (various words for the same kind of documents), there is no open standard to visualize projects in an universal way
- Multi-stakeholder participation, open innovation, open government and Organization 3.0 are growth markets
- Multi-stakeholder participation works, but fails when lack of transparency and decent participation, open tools and open platforms are helping to improve and mainstream multi-stakeholder participation
- Open government needs an open standard for envisioning projects from idea to archive
- The open project map works for both conventional (waterfall) and iterative (agile) project development
- The micro version of the open project map is the worlds smallest project visualization tool

Development of the online platform for the open project map is important because: 
- No digital tool, then too less value
- Which collaboration tool offers a whole project overview at your fingertips?
- There is a fast growing market for multi stakeholder participation, citizen participation, open government etc.
- Tools as Gitlab and Github are to difficult for many people
- Wordpress is a success because it integrates with many third parties, most tools don’t do this, which creates  changes

Development of the open project map may be tricky because:
- I have no experience with coding or projects on Gitlab, Github or other development platforms (so please be helpful and a bit patient :)

Erik Koopman
December 2018
