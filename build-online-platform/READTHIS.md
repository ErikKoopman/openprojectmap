# Basic idea

The online collaboration platform should be as simple and lightweight as possible. 

The three main parts of the platform are:
1. Menu bar
1. Top bar
1. View port


## Menu bar

The purpose of the menu bar is:

- to provide links to third party tools that show up in the view port

The list (link) an micro map of the open project map are integrate in the menu bar.

## Top bar

The purpose of the top bar is:

- to provide basic information (about the chosen link in the menu bar and tool in the viewport)
- show links for login, alerts and/or contact
- future: show custom select box with optional templates for new document


## View port

The purpose of the viewport is:

- to integrate popular third party tools as:
    - Websites / blog pages
    - PDF
    - Gitlab docs
    - Etherpad Lite
    - MS Word
    - Google Docs
    - Other collaboration tools

Cleck here for all requirements (under construction)