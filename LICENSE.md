The open project map is freely available for eduducation, care, non-profit organizations and companies < 10 fte. 
For these organizations the license for the templates of the open project map is [Creative commons BY-SA-4.0](http://creativecommons.org/licenses/by-sa/4.0).

For for-profit organizations all templates of the open project map have copyright. A license and information is available via mail @ erikkoopman .nl

The license for the online collaboration platform will be decided in collaboration with active developers.